# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


#Ajout de 10 users en base
10.times do
  User.create do |u|
    u.first_name = Faker::Name.first_name;
    u.last_name = Faker::Name.last_name;
    u.email = Faker::Internet.email;
    u.phone_number = "0" + 9.times.map{rand(9)}.join();
    u.date_of_birth = rand(14600).days.ago;
  end
end