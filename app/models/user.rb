class User < ApplicationRecord
  validates :email, presence: true
  validates :phone_number, presence: true
  validates :date_of_birth, presence: true
  validates :last_name, presence: true
  validates :first_name, presence: true

  has_many :photos
  def full_name
    "#{last_name.upcase} #{first_name.capitalize}"
    end
end

