class Photo < ApplicationRecord
  validates :name, presence: true
  #validates :file, presence: true
  validates :user_id, presence: true
  mount_uploader :file, PhotoUploader
  belongs_to :user
end
