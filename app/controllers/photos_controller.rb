class PhotosController < ApplicationController
  before_action :load_user
  before_action :load_photo, only: [:show, :edit, :destroy]

  def index
    @user = User.find(params[:user_id])
    @photos = Photo.all.where("user_id": @user).all
    end

  def new
    @photo = Photo.new
    @photo.user_id = @user.id
  end

  def create
    authorized_attributes = params.require(:photo).permit(
        :name,
        :file,
        :user_id,
    )
    @photo = Photo.new authorized_attributes
    if @photo.save
      redirect_to(user_photos_path)
    else
      render 'new'
    end
  end

  def load_user
    @user = User.find params[:user_id]
  end

  def load_photo
    @photo = @user.photos.find(params[:id])
  end

  def update
    authorized_attributes = params.require(:photo).permit(
        :name,
        :file,
        :user_id,
    )
    if @photo.update_attributes authorized_attributes
      redirect_to(user_photo_path)
    else
      render 'edit'
    end
  end

  def destroy
    @photo.destroy
    redirect_to user_photos_path(@user  )
  end
end
