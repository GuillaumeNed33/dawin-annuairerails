class UsersController < ApplicationController
  before_action :load_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all.order(:last_name)
  end

  def new
    @user = User.new
  end

  def create
    authorized_attributes = params.require(:user).permit(
        :first_name,
        :last_name,
        :date_of_birth,
        :phone_number,
        :email
    )
    @user = User.new authorized_attributes
    if @user.save
      redirect_to(users_path)
    else
      render 'new'
    end
  end


  def load_user
    @user = User.find(params[:id])
  end

  def update
    authorized_attributes = params.require(:user).permit(
        :first_name,
        :last_name,
        :date_of_birth,
        :phone_number,
        :email
    )
    if @user.update_attributes authorized_attributes
      redirect_to(user_path)
    else
      render 'edit'
    end
  end

  def destroy
    @user.destroy
    redirect_to users_path
  end
end
